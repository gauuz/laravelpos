<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransaksiHeaderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksi_header', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('no_transaksi');
            $table->integer('total');
            $table->integer('persen_diskon');
            $table->string('cara_bayar');
            $table->string('cc_number');
            $table->integer('customer_cash');
            $table->integer('customer_cc');
            $table->integer('kembalian');
            $table->integer('id_user');
            $table->integer('id_store');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi_header');
    }
}
