<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyTransaksiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transaksi_headers', function (Blueprint $table) {
            $table->string('no_transaksi', 14)->default('0')->change();
        });

        Schema::table('transaksi_details', function (Blueprint $table) {
            $table->renameColumn('no_transaksi', 'id_transaksi_headers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transaksi_headers', function (Blueprint $table) {
            $table->integer('no_transaksi')->after('id');
            $table->dropColumn('id');
        });

        Schema::table('transaksi_headers', function (Blueprint $table) {
            $table->increments('id')->first();
        });

        Schema::table('transaksi_details', function (Blueprint $table) {
            $table->renameColumn('id_transaksi_headers', 'no_transaksi');
        });
    }
}
