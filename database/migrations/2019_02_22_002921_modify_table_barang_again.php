<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyTableBarangAgain extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('barangs', function(Blueprint $table) {
            $table->renameColumn('id_toko', 'id_store');
            $table->renameColumn('tipe_barang', 'jenis_barang');
            $table->dropColumn('id_satuan');
            $table->dropColumn('id_merek');
            $table->renameColumn('harga_jual', 'harga_modal');
            $table->renameColumn('harga_beli', 'persen_markup');
            $table->renameColumn('diskon_jual', 'harga_jual');
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('barangs', function(Blueprint $table) {
            $table->renameColumn('id_store', 'id_toko');
            $table->renameColumn('jenis_barang', 'tipe_barang');
            $table->integer('id_satuan')->after('updated_at');
            $table->integer('id_merek')->after('id_satuan');
            $table->renameColumn('harga_modal', 'harga_jual');
            $table->renameColumn('persen_markup', 'harga_beli');
            $table->renameColumn('harga_jual', 'diskon_jual');
            $table->timestamps()->after('harga_jual');
        });
    }
}
