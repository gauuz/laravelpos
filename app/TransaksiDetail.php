<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransaksiDetail extends Model
{
    protected $table = 'transaksi_details';
    protected $fillable = ['id_store', 'no_transaksi', 'id_barang', 'nama_barang', 'qty', 'harga_jual', 'subtotal'];

    public function header() {
        return $this->hasOne('App\TransaksiHeader', 'id', 'id_transaksi_headers');
    }
}
