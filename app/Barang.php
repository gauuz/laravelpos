<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    protected $table = 'barangs';
    protected $fillable = ['id_store', 'nama_barang', 'jenis_barang', 'stock', 'harga_modal', 'persen_markup', 'harga_jual'];

    public function store() {
        return $this->hasOne('App\Store', 'id', 'id_store');
    }
}





