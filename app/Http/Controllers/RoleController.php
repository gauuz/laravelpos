<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = (new Role)->select('role')->paginate(2);
        return response()->json($role, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $insert = (new Role)->fill([
            'role' => $request->input('role'),
            ]);
    
            if($insert->save()){
                $res['success'] = true;
                $res['result'] = 'Success add role';
            }else{
                $res['success'] = false;
                $res['result'] = 'Failed add role';
            }
    
            return response()->json($res, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = (new Role)->find($id);

        $update->fill([
        'role' => $request->input('role'),
        ]);

        if($update->save()){
            $res['success'] = true;
            $res['result'] = 'Success update role';
        }else{
            $res['success'] = false;
            $res['result'] = 'Failed update role';
        }

        return response()->json($res, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = (new Role)->destroy($id);
        if($delete){
            $res['success'] = true;
            $res['result'] = 'Success delete role';
        }else{
            $res['success'] = false;
            $res['result'] = 'Failed delete role';
        }

        return response()->json($res, 200);
    }
}
