<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = (new User)->with('store')->get();
        return response()->json($user, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $insert = (new User)->fill([
            'id_store' => $request->input('id_store'),
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => $request->input('password'),
            'role' => $request->input('role'),
            'blokir' => $request->input('blokir'),
            ]);
    
            if($insert->save()){
                $res['success'] = true;
                $res['result'] = 'Success add user';
            }else{
                $res['success'] = false;
                $res['result'] = 'Failed add user';
            }
    
            return response()->json($res, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = (new User)->find($id);

        $update->fill([
            'id_store' => $request->input('id_store'),
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => $request->input('password'),
            'role' => $request->input('role'),
            'blokir' => $request->input('blokir'),
        ]);

        if($update->save()){
            $res['success'] = true;
            $res['result'] = 'Success update user';
        }else{
            $res['success'] = false;
            $res['result'] = 'Failed update user';
        }

        return response()->json($res, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = (new User)->destroy($id);
        if($delete){
            $res['success'] = true;
            $res['result'] = 'Success delete store';
        }else{
            $res['success'] = false;
            $res['result'] = 'Failed delete store';
        }

        return response()->json($res, 200);
    }
}
