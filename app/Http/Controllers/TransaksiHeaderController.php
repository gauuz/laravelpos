<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TransaksiHeader;

class TransaksiHeaderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transaksi_header = (new TransaksiHeader)->with('detail')->get();
        return response()->json($transaksi_header, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $insert = (new TransaksiHeader)->fill([
            'no_transaksi' => $request->input('no_transaksi'),
            'total' => $request->input('total'),
            'persen_diskon' => $request->input('persen_diskon'),
            'cara_bayar' => $request->input('cara_bayar'),
            'cc_number' => $request->input('cc_number'),
            'customer_cash' => $request->input('customer_cash'),
            'customer_cc' => $request->input('customer_cc'),
            'kembalian' => $request->input('kembalian'),
            'id_user' => $request->input('id_user'),
            'id_store' => $request->input('id_store'),
            ]);
    
            if($insert->save()){
                $res['success'] = true;
                $res['result'] = 'Success add transaksi header';
            }else{
                $res['success'] = false;
                $res['result'] = 'Failed add transaksi header';
            }
    
            return response()->json($res, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = (new TransaksiHeader)->find($id);

        $update->fill([
            'no_transaksi' => $request->input('no_transaksi'),
            'total' => $request->input('total'),
            'persen_diskon' => $request->input('persen_diskon'),
            'cara_bayar' => $request->input('cara_bayar'),
            'cc_number' => $request->input('cc_number'),
            'customer_cash' => $request->input('customer_cash'),
            'customer_cc' => $request->input('customer_cc'),
            'kembalian' => $request->input('kembalian'),
            'id_user' => $request->input('id_user'),
            'id_store' => $request->input('id_store'),
        ]);

        if($update->save()){
            $res['success'] = true;
            $res['result'] = 'Success update transaksi header';
        }else{
            $res['success'] = false;
            $res['result'] = 'Failed update transaksi header';
        }

        return response()->json($res, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = (new TransaksiHeader)->destroy($id);
        if($delete){
            $res['success'] = true;
            $res['result'] = 'Success delete transaksi header';
        }else{
            $res['success'] = false;
            $res['result'] = 'Failed delete transaksi header';
        }

        return response()->json($res, 200);
    }
}
