<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HistoriBarang;

class HistoriBarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $histori = (new HistoriBarang)->select('id_store', 'id_barang', 'masuk', 'keluar')->paginate(2);
        return response()->json($histori, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $insert = (new HistoriBarang)->fill([
            'id_store' => $request->input('id_store'),
            'id_barang' => $request->input('id_barang'),
            'masuk' => $request->input('masuk'),
            'keluar' => $request->input('keluar'),
            ]);
    
            if($insert->save()){
                $res['success'] = true;
                $res['result'] = 'Success add histori';
            }else{
                $res['success'] = false;
                $res['result'] = 'Failed add histori';
            }
    
            return response()->json($res, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = (new HistoriBarang)->find($id);

        $update->fill([
            'id_store' => $request->input('id_store'),
            'id_barang' => $request->input('id_barang'),
            'masuk' => $request->input('masuk'),
            'keluar' => $request->input('keluar'),
        ]);

        if($update->save()){
            $res['success'] = true;
            $res['result'] = 'Success update histori';
        }else{
            $res['success'] = false;
            $res['result'] = 'Failed update histori';
        }

        return response()->json($res, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = (new HistoriBarang)->destroy($id);
        if($delete){
            $res['success'] = true;
            $res['result'] = 'Success delete histori';
        }else{
            $res['success'] = false;
            $res['result'] = 'Failed delete histori';
        }

        return response()->json($res, 200);
    }
}
