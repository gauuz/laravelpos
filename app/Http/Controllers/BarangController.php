<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Barang;

class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $barang = (new Barang)->with('store')->get();
        return response()->json($barang, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $insert = (new Barang)->fill([
        'id_store' => $request->input('id_store'),
        'nama_barang' => $request->input('nama_barang'),
        'jenis_barang' => $request->input('jenis_barang'),
        'stock' => $request->input('stock'),
        'harga_modal' => $request->input('harga_modal'),
        'persen_markup' => $request->input('persen_markup'),
        'harga_jual' => $request->input('harga_jual'),
        ]);

        if($insert->save()){
            $res['success'] = true;
            $res['result'] = 'Success add barang';
        }else{
            $res['success'] = false;
            $res['result'] = 'Failed add barang';
        }

        return response()->json($res, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = (new Barang)->find($id);

        $update->fill([
        'id_store' => $request->input('id_store'),
        'nama_barang' => $request->input('nama_barang'),
        'jenis_barang' => $request->input('jenis_barang'),
        'stock' => $request->input('stock'),
        'harga_modal' => $request->input('harga_modal'),
        'persen_markup' => $request->input('persen_markup'),
        'harga_jual' => $request->input('harga_jual'),
        ]);

        if($update->save()){
            $res['success'] = true;
            $res['result'] = 'Success update barang';
        }else{
            $res['success'] = false;
            $res['result'] = 'Failed update barang';
        }

        return response()->json($res, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = (new Barang)->destroy($id);
        if($delete){
            $res['success'] = true;
            $res['result'] = 'Success delete barang';
        }else{
            $res['success'] = false;
            $res['result'] = 'Failed delete barang';
        }

        return response()->json($res, 200);
    }
}




