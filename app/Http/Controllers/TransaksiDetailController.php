<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TransaksiDetail;

class TransaksiDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transaksi_detail = (new TransaksiDetail)->with('header')->get();
        return response()->json($transaksi_detail, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $insert = (new TransaksiDetail)->fill([
            'id_store' => $request->input('id_store'),
            'no_transaksi' => $request->input('no_transaksi'),
            'id_barang' => $request->input('id_barang'),
            'nama_barang' => $request->input('nama_barang'),
            'qty' => $request->input('qty'),
            'harga_jual' => $request->input('harga_jual'),
            'subtotal' => $request->input('subtotal'),
            ]);
    
            if($insert->save()){
                $res['success'] = true;
                $res['result'] = 'Success add store';
            }else{
                $res['success'] = false;
                $res['result'] = 'Failed add store';
            }
    
            return response()->json($res, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = (new TransaksiDetail)->find($id);

        $update->fill([
            'id_store' => $request->input('id_store'),
            'no_transaksi' => $request->input('no_transaksi'),
            'id_barang' => $request->input('id_barang'),
            'nama_barang' => $request->input('nama_barang'),
            'qty' => $request->input('qty'),
            'harga_jual' => $request->input('harga_jual'),
            'subtotal' => $request->input('subtotal'),
        ]);

        if($update->save()){
            $res['success'] = true;
            $res['result'] = 'Success update transaksi detail';
        }else{
            $res['success'] = false;
            $res['result'] = 'Failed update transaksi detail';
        }

        return response()->json($res, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = (new TransaksiDetail)->destroy($id);
        if($delete){
            $res['success'] = true;
            $res['result'] = 'Success delete transaksi detail';
        }else{
            $res['success'] = false;
            $res['result'] = 'Failed delete transaksi detail';
        }

        return response()->json($res, 200);
    }
}
