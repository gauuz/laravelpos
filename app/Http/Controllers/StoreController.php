<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Store;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;


class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $store = (new Store)->with(['barang', 'user'])->get();
        return response()->json($store, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $test = Auth::user();

        // return $test;

        $insert = (new Store)->fill([
            'nama_store' => $request->input('nama_store'),
            'tagline_store' => $request->input('tagline_store'),
            'address' => $request->input('address'),
            'phone' => $request->input('phone'),
            'expires_at' => $request->input('expires_at'),
            ]);
    
            if($insert->save()){
                $res['success'] = true;
                $res['result'] = 'Success add store';

                $id_store = $insert->id;
                $id_logged_in = Auth::user()->id;

                $update = (new User)->find($id_logged_in);
                $update->fill([
                    'id_store' => $id_store,
                    ]);
            
                    if($update->save()){
                        $res['success'] = true;
                        $res['result'] = 'Success update store';
                    }else{
                        $res['success'] = false;
                        $res['result'] = 'Failed update store';
                    }
            }else{
                $res['success'] = false;
                $res['result'] = 'Failed add store';
            }
    
            return response()->json($res, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = (new Store)->find($id);

        $update->fill([
        'nama_store' => $request->input('nama_store'),
        'tagline_store' => $request->input('tagline_store'),
        'address' => $request->input('address'),
        'phone' => $request->input('phone'),
        'expires_at' => $request->input('expires_at'),
        ]);

        if($update->save()){
            $res['success'] = true;
            $res['result'] = 'Success update store';
        }else{
            $res['success'] = false;
            $res['result'] = 'Failed update store';
        }

        return response()->json($res, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = (new Store)->destroy($id);
        if($delete){
            $res['success'] = true;
            $res['result'] = 'Success delete store';
        }else{
            $res['success'] = false;
            $res['result'] = 'Failed delete store';
        }

        return response()->json($res, 200);
    }
}
