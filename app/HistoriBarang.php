<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoriBarang extends Model
{
    protected $table = 'histori_barangs';
    protected $fillable = ['id_store', 'id_barang', 'masuk', 'keluar'];

    public function barang() {
        return $this->hasOne('App\Barang', 'id', 'id_barang');
    }

    public function store() {
        return $this->hasOne('App\Store', 'id', 'id_store');
    }
}
