<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransaksiHeader extends Model
{
    protected $table = 'transaksi_headers';
    protected $fillable = ['no_transaksi', 'total', 'persen_diskon', 'cara_bayar', 'cc_number', 'customer_cash', 'customer_cc', 'kembalian', 'id_user', 'id_store'];

    public function detail() {
        return $this->hasMany('App\TransaksiDetail', 'id_transaksi_headers', 'id');
    }
}
