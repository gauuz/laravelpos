<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    protected $table = 'stores';
    protected $fillable = ['nama_store', 'tagline_store', 'address', 'phone', 'expires_at'];

    public function barang() {
        return $this->hasMany('App\Barang', 'id_store', 'id');
    }

    public function user() {
        return $this->hasMany('App\User', 'id_store', 'id');
    }
}



