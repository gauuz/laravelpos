<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});









//latihan hari
Route::get('/cikopo', 'ContohController@index');
Route::get('/purbalingga', 'ContohController@purwakarta');


//laravel passport
Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');

Route::group(['middleware' => 'auth:api'], function(){
    Route::post('details', 'API\UserController@details');

    //tabel transaksi_headers
    Route::post('/transaksi_header', 'TransaksiHeaderController@store');
    Route::post('/transaksi_header/{id}', 'TransaksiHeaderController@update');
    Route::delete('/transaksi_header/{id}', 'TransaksiHeaderController@destroy');
    Route::get('/transaksi_header', 'TransaksiHeaderController@index');

    //tabel barangs
    Route::post('/barang', 'BarangController@store');
    Route::post('/barang/{id}', 'BarangController@update');
    Route::delete('/barang/{id}', 'BarangController@destroy');
    Route::get('/barang', 'BarangController@index');

    //tabel stores
    Route::post('/store', 'StoreController@store');
    Route::post('/store/{id}', 'StoreController@update');
    Route::delete('/store/{id}', 'StoreController@destroy');
    Route::get('/store', 'StoreController@index');

    //tabel histori_barangs
    Route::post('/histori_barang', 'HistoriBarangController@store');
    Route::post('/histori_barang/{id}', 'HistoriBarangController@update');
    Route::delete('/histori_barang/{id}', 'HistoriBarangController@destroy');
    Route::get('/histori_barang', 'HistoriBarangController@index');

    //tabel users
    Route::post('/user', 'UserController@store');
    Route::post('/user/{id}', 'UserController@update');
    Route::delete('/user/{id}', 'UserController@destroy');
    Route::get('/user', 'UserController@index');

    //tabel roles
    Route::post('/role', 'RoleController@store');
    Route::post('/role/{id}', 'RoleController@update');
    Route::delete('/role/{id}', 'RoleController@destroy');
    Route::get('/role', 'RoleController@index');

    //tabel transaksi_details
    Route::post('/transaksi_detail', 'TransaksiDetailController@store');
    Route::post('/transaksi_detail/{id}', 'TransaksiDetailController@update');
    Route::delete('/transaksi_detail/{id}', 'TransaksiDetailController@destroy');
    Route::get('/transaksi_detail', 'TransaksiDetailController@index');

});



